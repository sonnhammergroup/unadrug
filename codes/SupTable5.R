## UNADRUG Project - Sonnahmmer's group
## Author: Ines Rivero Garcia

## This code outputs Sup. Table 5 of the paper, matching drugs to diseases.

setwd("/home/ines/Desktop/unadrug/")
library(tidyverse)

# Load files
DrugsNonredFile = read.csv("data/DrugBank_FCtargets_nonredundant.csv", 
                           header = TRUE, sep = "\t")

maxl_Nonred = read.csv("data/maxlinkNonred99.tsv", header = TRUE, sep="\t")

DrugsKnownFile = read.csv("data/DrugBank_FCtargets_nonredundant_approved.csv",
                          header = TRUE, sep="\t")

maxl_Known = read.csv("data/maxlinkNonred99.tsv", header = TRUE, sep="\t")

diseaseFile = read.csv("data/DisGeNET_diseases_20genes_s95.csv", 
                       header = TRUE, sep  ="\t")

mapping = read.csv("data/DisGeNET_filtered_data_by_EnsemblID.csv", 
                   header = TRUE, sep = "\t")

load("data/BigDiseaseModules_s95_20genes_FC80")

# Functions
## Create drug or disease list
listCreator = function(x, y){ # x = File name, y = File type (Drugs or diseases)
  output = list()
  i = 1
  if(y=="drugs"){
    Items = unique(as.character(x$Name))
    for(d in Items){
      output[[i]] = as.character(x[x$Name == d, "Gene.ID"])
      i = i +1
    }
  }else{
    Items = unique(as.character(x$DiseaseName))
    for(d in Items){
      output[[i]] = unlist(strsplit(as.character(x[
        x$DiseaseName == d, "Genes"]), ", "))
      i = i + 1
    }
  }
  return(output)
}

diseaseRepetition = function(x){
  output = character()
  for(i in 1:length(x)){
    output = c(output, rep(names(x)[i], length(x[[i]])))
  }
  return(output)
}

IDtoEnsembl = function(x, map = mapping){
  return(as.character(mapping[mapping$Gene == x, "EnsemblID"]))
}

addModuleNr = function(x, moduleList = list_bigClusters){
  output = 0
  disease = x[1]
  gene = x[2]
  for(i in 1:length(list_bigClusters)){
    if(disease == names(list_bigClusters)[i]){
      for(n in 1:length(list_bigClusters[[i]])){
        if(is.element(gene, unlist(list_bigClusters[[i]][n]))){
          output = n
        }
      }
    }
  }
  return(output)
}

drugGeneMap = function(x,drugFile){ #x=result matrix, y = drug file
  drugSet = character()
  for(j in 1:nrow(drugFile)){
    if(as.character(drugFile[j, 7]) == x[2]){
       drugSet = c(drugSet, as.character(drugFile[j, "DrugBank.ID"]))
    }
  }
  drugSet = str_c(drugSet, collapse = ",")
  return(drugSet)
}

# Disease List
diseaseList = listCreator(x = diseaseFile, y="diseases")
names(diseaseList) = diseaseFile$DiseaseName

# Create mapping gene - disease
diseaseGenes = unlist(diseaseList)
diseaseGenes = sapply(diseaseGenes, IDtoEnsembl)
diseaseReps = diseaseRepetition(diseaseList)
result = as.data.frame(cbind(diseaseReps, diseaseGenes))
result = apply(result, 2, function(x) as.character(x))

# Add module nr to which the gene belongs
modulenr = apply(result, 1, function(x) addModuleNr(x))
result = cbind(result, modulenr)

# Add drugs mapping to each gene
drugsDirect = apply(result, 1, function(x) drugGeneMap(x, drugFile = DrugsNonredFile))
drugsKnown = apply(result, 1, function(x) drugGeneMap(x, drugFile = DrugsKnownFile))
drugsExtended = apply(result, 1, function(x) drugGeneMap(x, drugFile = maxl_Nonred))
drugsKnownExt = apply(result, 1, function(x) drugGeneMap(x, drugFile = maxl_Known))
result = cbind(result, drugsDirect, drugsKnown, drugsExtended, drugsKnownExt)

colnames(result) = c("Disease", "Gene", "ModuleNumber", "DrugsDirectTargets",
                     "DrugsDirectKnownTargets", "DrugsExtendedTargets", 
                     "DrugsKnownExtendedTargets")

write.table(result, "results/Sup_Table_5.tsv", col.names = TRUE, row.names = FALSE,
            quote = FALSE, sep = "\t")
import numpy as np
import pandas as pd
import networkx as nx
from scipy.stats import hypergeom
import statsmodels.stats.multitest as multi

## MAXLINK FUNCTION
def extend_fdr(drug_genes, network, out_file='/home/ines/Desktop/maxlink_output.tsv', fdr=0.05, overwrite=True,
               fdr_method='holm-sidak'):
    """
    Finds genes linked to drug targets and extends the drug targets with these
    :param fdr_method:
    :param fdr: float (optional)
        to set the false discovery rate
    :param overwrite: bool (optional)
        if true write output to file even if the file exists
    :param drug_genes: dict
        drugs as keys and sets of genes as values
    :param network: networkx.Graph
        network
    :param out_file: string (optional)
        location of the output file
    :return: pd.DataFrame
        with columns #Node and Drug
    """
   # if os.path.exists(out_file) and not overwrite:
   #     return pd.read_csv(out_file, header=0, sep='\t')

    with open(out_file, 'w') as out:
        out.write('#Node\tDrug\n')

    network_size = len(network)
    drug_genes_extended = []
    for drug in drug_genes.keys():
        drug_genes_set = {gene for gene in drug_genes[drug] if network.has_node(gene)}
        group_size = len(drug_genes_set)
        extended_genes = drug_genes_set.copy()

        # initiate the extended list by adding the drug targets with p_value = 0
        p_values = ['zero'] * len(extended_genes)
        drug_names = [drug] * len(extended_genes)
        drug_genes_extended.extend(list(zip(list(extended_genes), drug_names, p_values)))

        all_neighbors = {neighbor for gene in drug_genes_set for neighbor in network.neighbors(gene)}
        for candidate in all_neighbors:
            degree = network.degree(candidate)
            in_degree = len(set(network.neighbors(candidate)) & drug_genes_set)
            hypergeometric_p = hypergeom.sf(in_degree - 1, network_size, group_size, degree)
            drug_genes_extended.append([candidate, drug, hypergeometric_p])

    drug_genes_extended_df = pd.DataFrame(drug_genes_extended)
    drug_genes_extended_df.columns = ['#Node', 'Drug', 'p_value']

    # B-H correction for multiple hypotheses testing
    only_extended = drug_genes_extended_df[drug_genes_extended_df.p_value != 'zero']
    hyper_reject_null, hyper_fdr, _, __ = multi.multipletests(only_extended.p_value, method=fdr_method, alpha=fdr)
    only_extended_fdr = only_extended.loc[hyper_reject_null]
    extended_fdr_corrected = only_extended_fdr.append(drug_genes_extended_df[drug_genes_extended_df.p_value == 'zero'])
    extended_fdr_corrected.p_value[extended_fdr_corrected.p_value == 'zero'] = 0
    extended_df = extended_fdr_corrected[['#Node', 'Drug']].drop_duplicates()
    extended_df.to_csv(out_file, index=False, sep='\t')
    print(f'Wrote {extended_df.shape[0]} genes to {out_file}.')
    return extended_df
   

# CREATE DRUG-GENE DICTIONARY
## Open drug data frames
nonredundantFile = pd.read_csv('/home/ines/Desktop/Uranium/Desktop/unadrug/data/DrugBank_FCtargets_nonredundant.csv', delimiter = '\t')
knownFile = pd.read_csv('/home/ines/Desktop/Uranium/Desktop/unadrug/data/DrugBank_FCtargets_nonredundant_approved.csv', delimiter = '\t')

## Create drug:genes dictionary
### Get drug names (each name appearing once)
nonredundantDrugs = nonredundantFile['Name'].to_list()
nonredundantDrugs = list(set(nonredundantDrugs))

knownDrugs = knownFile['Name'].to_list()
knownDrugs = list(set(knownDrugs))

### Create dictionary with drug names as keys
nonred = {}
for drug in nonredundantDrugs:
    nonred[drug] = ''
    
known = {}
for drug in knownDrugs:
    known[drug] = ''

### Fill dictionaries with Ensembl.IDs of targets as values
for drug in nonred:
    genes = nonredundantFile[nonredundantFile['Name']==drug]['Ensembl.ID'].to_list()
    nonred[drug] = genes

### Fill dictionaries with Ensembl.IDs of targets as values
for drug in known:
    genes = knownFile[knownFile['Name']==drug]['Ensembl.ID'].to_list()
    known[drug] = genes

# LOAD FC NETWORK
fc = pd.read_csv('/home/ines/Desktop/Uranium/Desktop/unadrug/data/FC4.1_H.sapiens_compact_filtered99', delimiter = '\t')


## Transform fc df into a Network.X object
Net = nx.from_pandas_edgelist(fc, '2:Gene1', '3:Gene2', ['0:PFC', '1:FBS_max'])

# CALL MAXLINK
#extend_fdr(nonred, Net, out_file='/home/ines/Desktop/maxlink_output_nonredundant99_2.tsv')

extend_fdr(known, Net, out_file='/home/ines/Desktop/maxlink_output_known99_2.tsv')


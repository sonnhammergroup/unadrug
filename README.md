# Repository for study on network-based drug repurposing
by Inés Rivero-García, Miguel Castresana-Aguirre, Luca Guglielmo, Dimitri Guala, Erik L. L. Sonnhammer

## Getting the code and data
You can download all the files used in the analysis by cloning the repository:
`git clone https://bitbucket.org/sonnhammergroup/unadrug.git`

## Contents
This repository contains the codes, data files, result files and figures that are used in the aforementioned paper. The repository contents are structured in the following directories:

### Codes
Contains the R codes used for data processing and reproducing the paper figures:

* Data-preprocessing codes:
  * DrugbankToTTD.R: converts DrugBank IDs to CasNrs and to TTD Ids.
  * filterDisGeNET.R: cleans the DisGeNET database to create the disease set.
  * getDrugDiseaseMappingsTTD.R: convert TTD database file to DrugBank format.
  * removeOverlappingDrugs.R: remove drugs with the same targets, leaving only a group representative.
  * covidModuleExample.R: used to pre-process data for Fig. 4.
  * infomapDiseaseModules.R: uses infomap to find the disease modules in our disease data set.
  * maxlink_drugs.py: uses MaxLink to find the high-quality first neighbours of drug targets.


* Codes used to produce the paper figures: fig2paper.R, fig3paper.R, fig4paper.R, fig5paper.R, supfig1paper.R, supfig3paper.R.


### Data
Stores the raw and processed data files used in the paper.


* Raw data files:
  * covid_GordonEtAl.xlsx: Supplementary Table 1 from Gordon, D.E., Jang, G.M., Bouhaddou, M. et al. A SARS-CoV-2 protein interaction map reveals targets for drug repurposing. Nature 583, 459–468 (2020). https://doi.org/10.1038/s41586-020-2286-9
  * FC4.1_H.sapiens_compact: FunCoup v4.1 _H. sapiens_ data base.
  * FC4.1_H.sapiens_compact_filtered80: FunCoup v4.1 _H. sapiens_ data base containing those protein-protein interactions with _pfc_ $\geq$ 0.80.
  * FC4.1_H.sapiens_compact_filtered99: FunCoup v4.1 _H. sapiens_ data base containing those protein-protein interactions with _pfc_ $\geq$ 0.99.
  * Uniprot_links_updated.csv: DrugBank data base file with Uniprot and Ensembl target IDs.


* Processed data files:
  * DrugBank_FCtargets_nonredundant.csv: processed drug data set.
  * DrugBank_FCtargets_nonredundant_approved.csv: processed drug data set only with pharmacologically characterized targets.
  * FDADrugDiseaseApprovals.csv: list of drug-disease indications approved by the FDA.
  * maxlinkNonred99.tsv: drug set with extended targets.
  * maxlinkKnown99.tsv: drug set with extended targets using only the pharmacologically characterized targets as seeds.
  * DisGeNET_diseases_20genes_s95.csv: processed disease set, mapping diseases with disease genes.
  * DisGeNET_filtered_data_by_disease.csv: diseases mapped to the gene symbols of their associated genes.
  * DisGeNET_filtered_data_by_EnsemblID.csv: list of diseases each gene is associated to.
  * BigDiseaseModules_s95_20genes_FC80: list of the disease modules and their genes found by Infomap.


### Results
Contains the result files and plots used during data analysis. The most relevant directories and files are:


* Results: contains Supplementary tables 1-3 (rankings of drugs based on their number of targets, diseases and disease modules).


* COVID19Example.cys: used to draw the COVID19 modules netwokr image.


* BarplotsDrugTargettingModules: used to create figure 5 and supplementary figure 4.

* Files used to create the networks in Fig. 5: ChildhoodAcuteLymphoblasticLeukemiaModule80.csv and UterineCervicalNeoplasmModule80.csv.


### Figures
Contains the svg files used to create the paper figures.
